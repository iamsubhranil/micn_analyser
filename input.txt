// number of ip/op
8
// number of stages (S)
3
// for each ip, S + 2 routes from ip to op
// ith column jth row denotes port j at stage i
0   0   0   0   0
1   4   4   1   1
2   2   1   4   2
3   6   5   5   3
4   1   2   2   4
5   5   6   3   5
6   3   3   6   6
7   7   7   7   7
