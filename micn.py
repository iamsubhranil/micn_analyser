import queue

lines = []

print("Parsing file..")

with open("input.txt", "r") as fp:
    for line in fp.readlines():
        # if line starts with '//', it's a comment line
        if not line.startswith("//"):
            lines.append(line)

if len(lines) == 0:
    exit(1)

# First line contains number of inputs
inputs = int(lines[0].strip())
# Second line contains number of stages
stages = int(lines[1].strip())
# Next 'inputs' number of lines contains
# 'stages + 2' numbers
mat = []
for i in range(inputs):
    line = lines[i + 2]
    line = line.replace("   ", " ").strip().split(" ")
    conn = []
    for idx in line:
        conn.append(int(idx))
    mat.insert(conn[0], conn)
# Now since we're gonna build a graph,
# we need to give the port of each stage
# a distinct number to make it a distinct
# vertex
denoter = 0
for row in mat:
    denoter = 0
    for i in range(len(row)):
        row[i] = row[i] + denoter
        denoter = denoter + 10

print("Building incidence matrix..")
# We could've used matrix here, but since
# we number the vertices with +10 increment
# each stage, it would be a pretty long
# matrix
incident_matrix = {}
for row in mat:
    for i in range(len(row) - 1):
        if row[i] not in incident_matrix:
            incident_matrix[row[i]] = []
        if row[i + 1] not in incident_matrix[row[i]]:
            incident_matrix[row[i]].append(row[i + 1])
        if i > 0:
            # mark the (i+1)th node as reachable from
            # {row[i] +- (row[i] mod 2)}th node also
            brother = row[i] + 1
            if row[i] % 2 == 1:
                brother = row[i] - 1
            if brother not in incident_matrix:
                incident_matrix[brother] = []
            if row[i + 1] not in incident_matrix[brother]:
                incident_matrix[brother].append(row[i + 1])

# Perform BFS
def bfs(incident_matrix, start, end):
    q = []
    visited = {}
    path = {}
    q.append(start)
    while len(q) > 0:
        vertex = q.pop()
        visited[vertex] = 1
        adjacent = []
        if vertex in incident_matrix:
            adjacent = incident_matrix[vertex]
        for v in adjacent:
            if v not in visited:
                visited[v] = 1
                path[v] = vertex
                if v == end:
                    return path
                q.append(v)

    return path

# Converts the path array in linear form
def construct_path(path, start, end):
    if start == end:
        return [start]
    linear_path = construct_path(path, start, path[end])
    linear_path.append(end)
    return linear_path

# Prints the path from a linear path array
def print_path(path):
    global stages
    print("Input port " + str(path[0]), end = '')
    for end in path[1:]:
        st = int(end / 10)
        sw = int((end % 10) / 2)
        pretty = ""
        if st <= stages:
            pretty = "Stage %d Switch %d Port %d" % (st, sw + 1, end % 2)
        else:
            pretty = "Output port %d" % (end % 2)
        print(" -> " + pretty, end='')

source1 = int(input("Enter source port for first path : "))
dest1 = int(input("Enter destination port for first path : "))
dest1 = dest1 + denoter - 10

source2 = int(input("Enter source port for second path : "))
dest2 = int(input("Enter destination port for second path : "))
dest2 = dest2 + denoter - 10
print("Calculating paths..")
path1 = bfs(incident_matrix, source1, dest1)
path2 = bfs(incident_matrix, source2, dest2)
path1 = construct_path(path1, source1, dest1)
path2 = construct_path(path2, source2, dest2)
print("Path for port %d to %d : " % (source1, dest1 % 10), end = '')
print_path(path1)
print()
print("Path for port %d to %d : " % (source2, dest2 % 10), end = '')
print_path(path2)
print()
print("Checking for collisions..")
col = False
for i in range(len(path1) - 1):
    sw1 = int(((path1[i] % 10) / 2) + 1)
    sw2 = int(((path2[i] % 10) / 2) + 1)
    port1 = int(path1[i + 1] % 2)
    port2 = int(path2[i + 1] % 2)
    nsw1 = int(((path1[i + 1] % 10) / 2) + 1)
    nsw2 = int(((path2[i + 1] % 10) / 2) + 1)
    if (sw1 == sw2) and ((nsw1 != nsw2) or ((nsw1 == nsw2) and (port1 != port2))):
        st = path1[i] / 10
        print("[Error] Collision at stage %d switch %d!" % (st, sw1))
        if i < stages:
            print("""First path requires stage %d switch %d input port %d
Second path requires stage %d switch %d input port %d!""" % (st + 1, nsw1, port1, st + 1, nsw2, port2))
        else:
            print("First path requires output port %d\nSecond path requires output port %d!" % (port1, port2))
        col = True
        break

if not col:
    print("The paths don't collide!")
